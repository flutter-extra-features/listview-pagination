import 'package:flutter/material.dart';
import 'package:listview_pagination_app/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}

/*
  Packages Used :-
    1] flutter pub add dio
    2] flutter pub add flutter_spinkit    ==> A collection of loading indicators animated with flutter

  Used API from this site https://dummyjson.com/docs/products#products-all
 */